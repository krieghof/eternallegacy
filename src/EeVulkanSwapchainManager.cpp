// SPDX-FileCopyrightText: (C) 2021 Manuel Wassermann <manuel.wassermann97@gmail.com>
// SPDX-License-Identifier: MIT

#include "EeVulkanSwapchainManager.h"

#include <cstdio>
#include <cstdlib>
#include <functional>
#include <vulkan/vulkan_core.h>

/* --------------------------------------------------- Constructor -------------------------------------------------- */
EeVulkanSwapchainManager::EeVulkanSwapchainManager(const EeWindow *window, const EeVulkanDeviceManager *device_manager)
    : window_(window), device_manager_(device_manager)
{
    swapchain_ = createSwapchain();
    renderPass_ = createRenderPass(device_manager_);
    swapchain_image_views_ = createImageViews(swapchain_);
    swapchain_framebuffers_ = createFramebuffer(swapchain_image_views_, renderPass_);

    // TODO: Maybe move fences and semaphores in a separate class
    createFences();
    createSemaphores();
}

EeVulkanSwapchainManager::~EeVulkanSwapchainManager()
{
    for (int i = 0; i < maxFrames; ++i)
    {
        vkDestroySemaphore(device_manager_->GetDevice(), imageAvailableSemaphores[i], nullptr);
        vkDestroySemaphore(device_manager_->GetDevice(), renderFinishedSemaphores[i], nullptr);
        vkDestroyFence(device_manager_->GetDevice(), inFlightFences[i], nullptr);
    }

    for (int i = 0; i < swapchain_framebuffers_.size(); ++i)
    {
        vkDestroyFramebuffer(device_manager_->GetDevice(),swapchain_framebuffers_[i], nullptr);
        vkDestroyImageView(device_manager_->GetDevice(), swapchain_image_views_[i], nullptr);
    }

    vkDestroyRenderPass(device_manager_->GetDevice(), renderPass_, nullptr);
    vkDestroySwapchainKHR(device_manager_->GetDevice(), swapchain_, nullptr);
}

/* ------------------------------------------------- Private Methods ------------------------------------------------ */
void EeVulkanSwapchainManager::createFences()
{
    inFlightFences.resize(maxFrames);
    imagesInFLight.resize(swapchain_image_views_.size(), VK_NULL_HANDLE);

    // Fence
    VkFenceCreateInfo fenceCreateInfo{};
    fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceCreateInfo.pNext = nullptr;
    fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (int i = 0; i < maxFrames; ++i)
    {
        if (vkCreateFence(device_manager_->GetDevice(), &fenceCreateInfo, nullptr, &inFlightFences[i]) != VK_SUCCESS)
        {
            fprintf(stderr, "Error in vkCreateFence");
            exit(EXIT_FAILURE);
        }
    }
}

void EeVulkanSwapchainManager::createSemaphores()
{
    imageAvailableSemaphores.resize(maxFrames);
    renderFinishedSemaphores.resize(maxFrames);

    // Semaphore
    VkSemaphoreCreateInfo semaphoreCreateInfo{};
    semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    semaphoreCreateInfo.pNext = nullptr;
    semaphoreCreateInfo.flags = 0;

    for (int i = 0; i < 2; ++i)
    {
        if (vkCreateSemaphore(device_manager_->GetDevice(), &semaphoreCreateInfo, nullptr,
                              &imageAvailableSemaphores[i]) != VK_SUCCESS ||
            vkCreateSemaphore(device_manager_->GetDevice(), &semaphoreCreateInfo, nullptr,
                              &renderFinishedSemaphores[i]) != VK_SUCCESS)
        {
            fprintf(stderr, "Error in vkCreateSemaphore");
            exit(EXIT_FAILURE);
        }
    }
}

VkSwapchainKHR EeVulkanSwapchainManager::createSwapchain()
{
    VkSwapchainKHR swapchain = VK_NULL_HANDLE;

    surface_format_ = device_manager_->GetSurfaceFormats()[0]; // TODO: Pick the best surface format
    present_mode_ = VK_PRESENT_MODE_FIFO_KHR;                  // TODO: Pick the best present mode from device_manager_
    extent_ = window_->QueryFramebufferSize();                 // INFO: Sometimes this causes a validation layer message when resizing

    uint32_t image_count = device_manager_->GetSurfaceCapabilities().minImageCount + 1;
    // If maxImageCount == 0 we have no maximum
    if (device_manager_->GetSurfaceCapabilities().maxImageCount > 0 &&
        image_count > device_manager_->GetSurfaceCapabilities().maxImageCount)
    {
        image_count = device_manager_->GetSurfaceCapabilities().maxImageCount;
    }

    VkSwapchainCreateInfoKHR create_info{};
    create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    create_info.pNext = nullptr;
    create_info.flags = 0;
    create_info.surface = window_->GetSurface();
    create_info.minImageCount = image_count;
    create_info.imageFormat = surface_format_.format;
    create_info.imageColorSpace = surface_format_.colorSpace;
    create_info.imageExtent = extent_;
    create_info.imageArrayLayers = 1;
    create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE; // TODO: Set the correct mode for the used device
    create_info.queueFamilyIndexCount = 0; // TODO: When we have more then one family --- if graphicsFamily != presentFamily)
    create_info.pQueueFamilyIndices = nullptr;
    create_info.preTransform = device_manager_->GetSurfaceCapabilities().currentTransform;
    create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    create_info.presentMode = present_mode_;
    create_info.clipped = VK_TRUE;
    create_info.oldSwapchain = VK_NULL_HANDLE;

    if (vkCreateSwapchainKHR(device_manager_->GetDevice(), &create_info, nullptr, &swapchain))
    {
        fprintf(stderr, "Error in vkCreateSwapchainKHR");
        exit(EXIT_FAILURE);
    }
    return swapchain;
}

VkRenderPass EeVulkanSwapchainManager::createRenderPass(const EeVulkanDeviceManager *deviceManager)
{
    VkRenderPass render_pass = VK_NULL_HANDLE;

    /* --------- Sub Pass --------- */
    VkAttachmentDescription color_attachment{};
    color_attachment.flags = 0;
    color_attachment.format = surface_format_.format;
    color_attachment.samples = VK_SAMPLE_COUNT_1_BIT;
    color_attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    color_attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    color_attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    color_attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    color_attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    color_attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    VkAttachmentReference color_attachment_reference{};
    color_attachment_reference.attachment = 0; // Index
    color_attachment_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass_description{};
    subpass_description.flags = 0;
    subpass_description.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass_description.inputAttachmentCount = 0;
    subpass_description.pInputAttachments = nullptr;
    subpass_description.colorAttachmentCount = 1;
    subpass_description.pColorAttachments = &color_attachment_reference;
    subpass_description.pResolveAttachments = nullptr;
    subpass_description.pDepthStencilAttachment = nullptr;
    subpass_description.preserveAttachmentCount = 0;
    subpass_description.pPreserveAttachments = nullptr;

    VkSubpassDependency dependency{};
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;
    dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.srcAccessMask = 0;
    dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    /* --------- Render Pass --------- */
    VkRenderPassCreateInfo render_pass_create_info{};
    render_pass_create_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    render_pass_create_info.pNext = nullptr;
    render_pass_create_info.flags = 0;
    render_pass_create_info.attachmentCount = 1;
    render_pass_create_info.pAttachments = &color_attachment;
    render_pass_create_info.subpassCount = 1;
    render_pass_create_info.pSubpasses = &subpass_description;
    render_pass_create_info.dependencyCount = 1;
    render_pass_create_info.pDependencies = &dependency;

    if (vkCreateRenderPass(deviceManager->GetDevice(), &render_pass_create_info, nullptr, &render_pass) != VK_SUCCESS)
    {
        fprintf(stderr, "Error in vkCreateRenderPass");
        exit(EXIT_FAILURE);
    }
    return render_pass;
}

std::vector<VkImageView> EeVulkanSwapchainManager::createImageViews(VkSwapchainKHR swapChain)
{
    // Get swap chain images
    uint32_t image_count;
    vkGetSwapchainImagesKHR(device_manager_->GetDevice(), swapChain, &image_count, nullptr);
    std::vector<VkImage> swapChainImages(image_count);
    vkGetSwapchainImagesKHR(device_manager_->GetDevice(), swapChain, &image_count, swapChainImages.data());

     // Create image views
     std::vector<VkImageView> swapChainImageViews(swapChainImages.size());
    for (size_t i = 0; i < swapChainImages.size(); ++i)
    {
        VkImageViewCreateInfo view_create_info{};
        view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        view_create_info.pNext = nullptr;
        view_create_info.flags = 0;
        view_create_info.image = swapChainImages[i];
        view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        view_create_info.format = surface_format_.format;
        view_create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        view_create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        view_create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        view_create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        view_create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        view_create_info.subresourceRange.baseMipLevel = 0;
        view_create_info.subresourceRange.levelCount = 1;
        view_create_info.subresourceRange.baseArrayLayer = 0;
        view_create_info.subresourceRange.layerCount = 1;

        if (vkCreateImageView(device_manager_->GetDevice(), &view_create_info, nullptr, &swapChainImageViews[i]) !=
            VK_SUCCESS)
        {
            fprintf(stderr, "Error in vkCreateImageView");
            exit(EXIT_FAILURE);
        }
    }
    return swapChainImageViews;
}

std::vector<VkFramebuffer> EeVulkanSwapchainManager::createFramebuffer(std::vector<VkImageView> imageViews,
                                                                       VkRenderPass renderPass)
{
    std::vector<VkFramebuffer> swap_chain_framebuffers(imageViews.size());
    for (size_t i = 0; i < swap_chain_framebuffers.size(); ++i)
    {
        VkImageView attachments[] = {imageViews[i]};

        VkFramebufferCreateInfo framebuffer_create_info{};
        framebuffer_create_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebuffer_create_info.pNext = nullptr;
        framebuffer_create_info.flags = 0;
        framebuffer_create_info.renderPass = renderPass;
        framebuffer_create_info.attachmentCount = 1;
        framebuffer_create_info.pAttachments = attachments;
        framebuffer_create_info.width = extent_.width;
        framebuffer_create_info.height = extent_.height;
        framebuffer_create_info.layers = 1;

        if (vkCreateFramebuffer(device_manager_->GetDevice(), &framebuffer_create_info, nullptr,
                                &swap_chain_framebuffers[i]) != VK_SUCCESS)
        {
            fprintf(stderr, "Error in vkCreateFramebuffer");
            exit(EXIT_FAILURE);
        }
    }
    return swap_chain_framebuffers;
}

/* ------------------------------------------------- Public Methods ------------------------------------------------- */
VkResult EeVulkanSwapchainManager::AcquireNextImage(uint32_t &imageIndex)
{
    vkWaitForFences(device_manager_->GetDevice(), 1, &inFlightFences[currentFrame], VK_TRUE, UINT64_MAX);

    return vkAcquireNextImageKHR(device_manager_->GetDevice(), swapchain_, UINT64_MAX, imageAvailableSemaphores[currentFrame],
                          VK_NULL_HANDLE, &imageIndex);
}

VkResult EeVulkanSwapchainManager::SubmitCommandBuffers(const VkCommandBuffer &buffers, uint32_t &imageIndex)
{
    // Check if a previous frame is using this image (i.e. there is its fence to wait on)
    if (imagesInFLight[imageIndex] != VK_NULL_HANDLE)
    {
        vkWaitForFences(device_manager_->GetDevice(), 1, &imagesInFLight[imageIndex], VK_TRUE, UINT64_MAX);
    }
    // Mark the image as now being in use by this frame
    imagesInFLight[imageIndex] = inFlightFences[currentFrame];

    /* Queue Submit */
    VkSemaphore wait_semaphores[] = {imageAvailableSemaphores[currentFrame]};
    VkSemaphore signal_semaphores[] = {renderFinishedSemaphores[currentFrame]};
    VkPipelineStageFlags wait_stages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};

    VkSubmitInfo submit_info{};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = nullptr;
    submit_info.waitSemaphoreCount = 1;
    submit_info.pWaitSemaphores = wait_semaphores;
    submit_info.pWaitDstStageMask = wait_stages;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &buffers;
    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores = signal_semaphores;

    vkResetFences(device_manager_->GetDevice(), 1, &inFlightFences[currentFrame]);

    if (vkQueueSubmit(device_manager_->GetGraphicsQueue(), 1, &submit_info, inFlightFences[currentFrame]) != VK_SUCCESS)
    {
        fprintf(stderr, "Error in vkQueueSubmit");
        exit(EXIT_FAILURE);
    }

    /* Presentation */
    VkSwapchainKHR swap_chains[] = {swapchain_};
    VkPresentInfoKHR present_info{};
    present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    present_info.pNext = nullptr;
    present_info.waitSemaphoreCount = 1;
    present_info.pWaitSemaphores = signal_semaphores;
    present_info.swapchainCount = 1;
    present_info.pSwapchains = swap_chains;
    present_info.pImageIndices = &imageIndex;
    present_info.pResults = nullptr;

    VkResult result = vkQueuePresentKHR(device_manager_->GetPresentQueue(), &present_info);
    currentFrame = (currentFrame + 1) % maxFrames;
    return result;
}
