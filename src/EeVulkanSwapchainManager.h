// SPDX-FileCopyrightText: (C) 2021 Manuel Wassermann <manuel.wassermann97@gmail.com>
// SPDX-License-Identifier: MIT

#pragma once

#include "EeVulkanDeviceManager.h"
#include "EeWindow.h"

#include <vector>

class EeVulkanSwapchainManager
{
    /* ----------------------------------------------- Member Variables ----------------------------------------------*/
  private:
    const EeWindow *window_;
    const EeVulkanDeviceManager *device_manager_;

    VkSwapchainKHR swapchain_ = VK_NULL_HANDLE;
    VkRenderPass renderPass_ = VK_NULL_HANDLE;

    std::vector<VkImageView> swapchain_image_views_;
    std::vector<VkFramebuffer> swapchain_framebuffers_;

    VkSurfaceFormatKHR surface_format_ = {};
    VkPresentModeKHR present_mode_ = {};
    VkExtent2D extent_ = {};

    const int maxFrames = 2;
    size_t currentFrame = 0;
    std::vector<VkSemaphore> imageAvailableSemaphores;
    std::vector<VkSemaphore> renderFinishedSemaphores;
    std::vector<VkFence> inFlightFences;
    std::vector<VkFence> imagesInFLight;

    /* -------------------------------------------------- Constructor ------------------------------------------------*/
  public:
    EeVulkanSwapchainManager(const EeWindow *window, const EeVulkanDeviceManager *device_manager);
    ~EeVulkanSwapchainManager();

    EeVulkanSwapchainManager(const EeVulkanSwapchainManager &) = delete;
    EeVulkanSwapchainManager &operator=(const EeVulkanSwapchainManager &) = delete;

    /* ------------------------------------------------ Private Methods ----------------------------------------------*/
  private:
    VkExtent2D pickSwapExtent();
    VkSwapchainKHR createSwapchain();
    VkRenderPass createRenderPass(const EeVulkanDeviceManager *deviceManager);
    std::vector<VkImageView> createImageViews(VkSwapchainKHR swapChain);
    std::vector<VkFramebuffer> createFramebuffer(std::vector<VkImageView> imageViews, VkRenderPass renderPass);

    void createFences();
    void createSemaphores();


    /* ------------------------------------------------ Public Methods -----------------------------------------------*/
  public:
    VkResult AcquireNextImage(uint32_t &imageIndex);
    VkResult SubmitCommandBuffers(const VkCommandBuffer &buffers, uint32_t &imageIndex);

    /* ------------------------------------------------ Getter Methods -----------------------------------------------*/
  public:
    const VkExtent2D &GetExtent() const { return extent_; }
    const VkSurfaceFormatKHR &GetSurfaceFormat() const { return surface_format_; }
    const std::vector<VkImageView> &GetSwapchainImageViews() const { return swapchain_image_views_; }
    const VkSwapchainKHR &GetSwapchain() const { return swapchain_; }
    const VkRenderPass &GetRenderPass() const { return renderPass_; }
    const std::vector<VkFramebuffer> &GetFramebuffers() const { return swapchain_framebuffers_; }
};
