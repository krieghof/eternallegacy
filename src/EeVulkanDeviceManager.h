// SPDX-FileCopyrightText: (C) 2021 Manuel Wassermann <manuel.wassermann97@gmail.com>
// SPDX-License-Identifier: MIT

#pragma once

#include "EeWindow.h"

#include <cstdio>
#include <cstdlib>
#include <vector>
#include <vulkan/vulkan.h>

class EeVulkanDeviceManager
{
    struct DeviceInfo
    {
        VkPhysicalDevice device;
        VkPhysicalDeviceProperties properties;
        VkPhysicalDeviceFeatures features;
        std::vector<VkExtensionProperties> extension_properties;
        std::vector<VkQueueFamilyProperties> queue_family_properties;

        // Surface - Those properties get only queried if a VkSurfaceKHR is provided
        VkSurfaceCapabilitiesKHR surface_capabilities;
        std::vector<VkSurfaceFormatKHR> surface_formats;
        std::vector<VkPresentModeKHR> surface_present_modes;
        std::vector<VkBool32> queue_family_presentation_support;
    };

    /* ----------------------------------------------- Member Variables ----------------------------------------------*/
  private:
    inline static std::vector<DeviceInfo> devices_;
    inline static uint32_t num_devices_;

    uint32_t device_index_;
    uint32_t queue_family_index_;
    VkDevice logical_device_ = VK_NULL_HANDLE;

    VkQueue graphics_queue_ = VK_NULL_HANDLE;
    VkQueue present_queue_ = VK_NULL_HANDLE;

    VkCommandPool commandPool_ = VK_NULL_HANDLE;

    /* -------------------------------------------------- Constructor ------------------------------------------------*/
  public:
    explicit EeVulkanDeviceManager(const VkInstance &instance, const VkSurfaceKHR &surface)
    {
        devices_ = queryPhysicalDevices(instance);
        num_devices_ = devices_.size();

        queryPhysicalDevicesInfos(surface);

        device_index_ = getDefaultPhysicalDeviceIndex(surface);
        queue_family_index_ = getDefaultQueueFamilyIndex(surface);
        logical_device_ = createDevice();

        commandPool_ = createCommandPool();

        // TODO: Find the correct queue for graphics and present
        vkGetDeviceQueue(logical_device_, queue_family_index_, 0, &graphics_queue_);
        vkGetDeviceQueue(logical_device_, queue_family_index_, 0, &present_queue_);
    }

    /* -------------------------------------------------- Destructor -------------------------------------------------*/
    ~EeVulkanDeviceManager()
    {
        if (commandPool_ != VK_NULL_HANDLE) {
            vkDestroyCommandPool(logical_device_,commandPool_, nullptr);
        }
        vkDestroyDevice(logical_device_, nullptr);
    }

    EeVulkanDeviceManager(const EeVulkanDeviceManager &) = delete;
    EeVulkanDeviceManager &operator=(const EeVulkanDeviceManager &) = delete;

    /* ------------------------------------------------ Private Methods ----------------------------------------------*/
  private:
    static std::vector<DeviceInfo> queryPhysicalDevices(const VkInstance &instance);
    void queryPhysicalDevicesInfos(const VkSurfaceKHR &surface);
    const uint32_t getDefaultPhysicalDeviceIndex(const VkSurfaceKHR &surface) const;
    const uint32_t getDefaultQueueFamilyIndex(const VkSurfaceKHR &surface) const;

    VkDevice createDevice();
    VkCommandPool createCommandPool();

    /* ------------------------------------------------ Getter Methods -----------------------------------------------*/
  public:
    /**
     *
     * @return active VkPhysicalDevice or nullptr on error
     */
    VkPhysicalDevice GetPhysicalDevice() const { return devices_[device_index_].device; }

    /**
     *
     * @return is the VkDevice
     */
    const VkDevice &GetDevice() const { return logical_device_; }

    /**
     *
     * @return is the graphics queue
     */
    const VkQueue &GetGraphicsQueue() const { return graphics_queue_; }

    /**
     *
     * @return is the present queue
     */
    const VkQueue &GetPresentQueue() const { return present_queue_; }

    /**
     *
     * @return number of installed physical devices
     */
    const uint32_t GetPhysicalDeviceCount() const { return num_devices_; }

    /**
     *
     * @return is the index of the currently active physical device
     */
    const uint32_t GetPhysicalDeviceIndex() const { return device_index_; }

    /**
     *
     * @return is the queue family index
     */
    const uint32_t GetQueueFamilyIndex() const { return queue_family_index_; }

    /**
     *
     * @returns the physical device properties from the currently active physical device
     */
    const VkPhysicalDeviceProperties &GetPhysicalDeviceProperties() const { return devices_[device_index_].properties; }

    /**
     *
     * @returns the physical device features from the currently active physical device
     */
    const VkPhysicalDeviceFeatures &GetPhysicalDeviceFeatures() const { return devices_[device_index_].features; }

    /**
     *
     * @returns the queue family properties from the currently active queue family
     */
    const std::vector<VkQueueFamilyProperties> &GetQueueFamilyProperties() const { return devices_[device_index_].queue_family_properties; }

    /**
     *
     * @return VkSurfaceCapabilitiesKHR of the active physical device
     */
    const VkSurfaceCapabilitiesKHR &GetSurfaceCapabilities() const { return devices_[device_index_].surface_capabilities; }
    /**
     *
     * @return vector<VkSurfaceFormatKHR> of the active physical device
     */
    const std::vector<VkSurfaceFormatKHR> &GetSurfaceFormats() const { return devices_[device_index_].surface_formats; }
    /**
     *
     * @return the command pool
     */
    const VkCommandPool& GetCommandPool() const {return commandPool_; };
};
