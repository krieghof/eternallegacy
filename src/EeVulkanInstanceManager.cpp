// SPDX-FileCopyrightText: (C) 2021 Manuel Wassermann <manuel.wassermann97@gmail.com>
// SPDX-License-Identifier: MIT

#include "EeVulkanInstanceManager.h"

#include <cstring>

/* --------------------------------------------------- Constructor -------------------------------------------------- */
EeVulkanInstanceManager::EeVulkanInstanceManager(const char *applicationName, const int width, const int height)
    : applicationName_(applicationName)
{
    if (width > 0 && height > 0)
        window_ = new EeWindow(width, height, applicationName);

    instance_ = createInstance();

    if (window_ != nullptr)
        surface_ = window_->InitSurface(instance_);
}

EeVulkanInstanceManager::~EeVulkanInstanceManager()
{
    if (window_ != nullptr)
        window_->DestroySurface(instance_);

    vkDestroyInstance(instance_, nullptr);

    delete window_;
}

/* ------------------------------------------------- Private Methods ------------------------------------------------ */
const VkInstance EeVulkanInstanceManager::createInstance()
{
    VkInstance instance = VK_NULL_HANDLE;

    /* Application Info */
    VkApplicationInfo appInfo{};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = applicationName_;
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "Eternal";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    /* EeVulkanInstanceManager Create Info */
    VkInstanceCreateInfo instanceCreateInfo{};
    instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceCreateInfo.pApplicationInfo = &appInfo;

    // Validation Layers
    if (activateValidation_)
    {
        activeExtensions_.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME); // Add required extension for validation
        areLayersAvailable();
        instanceCreateInfo.enabledLayerCount = static_cast<uint32_t>(activeLayers_.size());
    }
    else
    {
        instanceCreateInfo.enabledLayerCount = 0;
    }

    instanceCreateInfo.ppEnabledLayerNames = activeLayers_.data();

    // Add the required extension for the window
    if (window_ != nullptr)
        for (auto extension : window_->GetInstanceExtensions())
            activeExtensions_.push_back(extension);

    areExtensionsAvailable();
    instanceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(activeExtensions_.size());
    instanceCreateInfo.ppEnabledExtensionNames = activeExtensions_.data();

    if (vkCreateInstance(&instanceCreateInfo, nullptr, &instance) != VK_SUCCESS)
    {
        fprintf(stderr, "Error while creating instance");
        exit(EXIT_FAILURE);
    }
    return instance;
}

void EeVulkanInstanceManager::areLayersAvailable()
{
    // TODO: Error check for layers
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
    std::vector<VkLayerProperties> layers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, layers.data());

    for (auto active_layer : activeLayers_)
    {
        bool layer_available = false;
        for (auto available_layer : layers)
        {
            if (strcmp(active_layer, available_layer.layerName) == 0)
            {
                layer_available = true;
                break;
            }
        }
        if (!layer_available)
        {
            fprintf(stderr, "MESSAGE: requested validation layer is not available \t layerName: %s", active_layer);
            exit(EXIT_FAILURE);
        }
    }
}

void EeVulkanInstanceManager::areExtensionsAvailable()
{
    // TODO: Error check for extensions
    uint32_t extensionCount;
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
    std::vector<VkExtensionProperties> extensions(extensionCount);
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());

    for (auto active_extension : activeExtensions_)
    {
        bool extension_available = false;
        for (auto available_extension : extensions)
        {
            if (strcmp(active_extension, available_extension.extensionName) == 0)
            {
                extension_available = true;
                break;
            }
        }
        if (!extension_available)
        {
            fprintf(stderr, "MESSAGE: requested extension is not available \t extensionName: %s", active_extension);
            exit(EXIT_FAILURE);
        }
    }
}
