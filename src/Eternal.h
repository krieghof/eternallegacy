// SPDX-FileCopyrightText: (C) 2021 Manuel Wassermann <manuel.wassermann97@gmail.com>
// SPDX-License-Identifier: MIT

#pragma once

#include "EeRenderer.h"

#include <cstdio>

class Eternal
{

    /* Member Variable */
  private:
    EeVulkanInstanceManager *instanceManager_;
    EeVulkanDeviceManager *deviceManager_;
    EeRenderer *renderer_;

    /* Constructor */
  public:
    Eternal()
    {
        // Create Vulkan Instance and device
        instanceManager_ = new EeVulkanInstanceManager("title", 800, 600);
        deviceManager_ = new EeVulkanDeviceManager(instanceManager_->GetInstance(), instanceManager_->GetSurface());

        // Create Vulkan Renderer
        renderer_ = new EeRenderer(instanceManager_->GetWindow(), deviceManager_);
    }

    ~Eternal()
    {
        delete renderer_;
        delete deviceManager_;
        delete instanceManager_;
    }

    /* Public Methods */
  public:
    void Run()
    {
        renderer_->Run();
    }
};
