// SPDX-FileCopyrightText: (C) 2021 Manuel Wassermann <manuel.wassermann97@gmail.com>
// SPDX-License-Identifier: MIT

#include "EeWindow.h"

/* --------------------------------------------------- Constructor -------------------------------------------------- */
EeWindow::EeWindow(const int width, const int height, const char *title)
{
    this->title_ = title;

    glfwInit();

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    window_ = glfwCreateWindow(width, height, title, nullptr, nullptr);
    glfwSetWindowUserPointer(window_, this);
    glfwSetFramebufferSizeCallback(window_, framebufferResizeCallback);

    glfw_extensions_ = glfwGetRequiredInstanceExtensions(&glfw_extension_count_);
}

EeWindow::~EeWindow()
{
    glfwDestroyWindow(window_);
    glfwTerminate();
}

/* ------------------------------------------------- Private Methods ------------------------------------------------ */

void EeWindow::framebufferResizeCallback(GLFWwindow *window, int width, int height) {
    auto eeWindow = reinterpret_cast<EeWindow *>(glfwGetWindowUserPointer(window));
    eeWindow->width_ = width;
    eeWindow->height_ = height;
}

/* ------------------------------------------------- Public Methods ------------------------------------------------- */
bool EeWindow::ShouldClose() const
{
    return glfwWindowShouldClose(window_);
}

void EeWindow::PollEvents()
{
    glfwPollEvents();
}

VkSurfaceKHR const &EeWindow::InitSurface(VkInstance &instance)
{
    if (surface_ == VK_NULL_HANDLE && instance != VK_NULL_HANDLE)
    {
        if (glfwCreateWindowSurface(instance, window_, nullptr, &surface_) != VK_SUCCESS)
        {
            fprintf(stderr, "Error while creating surface");
            exit(EXIT_FAILURE);
        }
    }
    return surface_;
}

void EeWindow::DestroySurface(VkInstance instance)
{
    if (instance != VK_NULL_HANDLE && surface_ != VK_NULL_HANDLE)
        vkDestroySurfaceKHR(instance, surface_, nullptr);
}

VkExtent2D EeWindow::QueryFramebufferSize() const
{
    int width, height;
    glfwGetFramebufferSize(window_, &width, &height);

    return {static_cast<uint32_t>(width), static_cast<uint32_t>(height)};
}