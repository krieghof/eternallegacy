// SPDX-FileCopyrightText: (C) 2021 Manuel Wassermann <manuel.wassermann97@gmail.com>
// SPDX-License-Identifier: MIT

#include "EeVulkanPipelineManager.h"

#include <fstream>

/* --------------------------------------------------- Constructor -------------------------------------------------- */
EeVulkanPipelineManager::EeVulkanPipelineManager(const EeVulkanSwapchainManager *swapchain_manager,
                                                 const EeVulkanDeviceManager *device_manager)
    : swapchain_manager_(swapchain_manager), device_manager_(device_manager)
{
    createGraphicsPipeline(); // needs render pass and device
}

/* --------------------------------------------------- Destructor --------------------------------------------------- */
EeVulkanPipelineManager::~EeVulkanPipelineManager()
{
    vkDestroyPipeline(device_manager_->GetDevice(), graphics_pipeline_, nullptr);
    vkDestroyPipelineLayout(device_manager_->GetDevice(), pipeline_layout_, nullptr);
}

/* ------------------------------------------------- Private Methods ------------------------------------------------ */

void EeVulkanPipelineManager::createGraphicsPipeline()
{
    /* --------- Create Shader Modules --------- */
    auto vert_shader_code = readFile("../shaders/vert.spv");
    auto frag_shader_code = readFile("../shaders/frag.spv");

    VkShaderModule vert_shader_module = createShaderModule(vert_shader_code);
    VkShaderModule frag_shader_module = createShaderModule(frag_shader_code);

    // Vertex Shader Stage
    VkPipelineShaderStageCreateInfo vert_stage_create_info{};
    vert_stage_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vert_stage_create_info.pNext = nullptr;
    vert_stage_create_info.flags = 0;
    vert_stage_create_info.stage = VK_SHADER_STAGE_VERTEX_BIT;
    ;
    vert_stage_create_info.module = vert_shader_module;
    vert_stage_create_info.pName = "main";
    vert_stage_create_info.pSpecializationInfo = nullptr;

    // Fragment Shader Stage
    VkPipelineShaderStageCreateInfo frag_stage_create_info{};
    frag_stage_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    frag_stage_create_info.pNext = nullptr;
    frag_stage_create_info.flags = 0;
    frag_stage_create_info.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    frag_stage_create_info.module = frag_shader_module;
    frag_stage_create_info.pName = "main";
    frag_stage_create_info.pSpecializationInfo = nullptr;

    VkPipelineShaderStageCreateInfo shader_stages[] = {vert_stage_create_info, frag_stage_create_info};

    /* --------- Fixed Functions --------- */
    // Vertex Input Info
    VkPipelineVertexInputStateCreateInfo vertex_input_info{};
    vertex_input_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertex_input_info.pNext = nullptr;
    vertex_input_info.flags = 0;
    vertex_input_info.vertexBindingDescriptionCount = 0;
    vertex_input_info.pVertexBindingDescriptions = nullptr;
    vertex_input_info.vertexAttributeDescriptionCount = 0;
    vertex_input_info.pVertexAttributeDescriptions = nullptr;

    // Input Assembly Info
    VkPipelineInputAssemblyStateCreateInfo assembly_input_info{};
    assembly_input_info.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    assembly_input_info.pNext = nullptr;
    assembly_input_info.flags = 0;
    assembly_input_info.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    assembly_input_info.primitiveRestartEnable = VK_FALSE;

    // Viewport State Create Info
    // Needs Viewport and Scissor
    VkPipelineViewportStateCreateInfo viewport_state_create_info{};
    viewport_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewport_state_create_info.pNext = nullptr;
    viewport_state_create_info.flags = 0;
    viewport_state_create_info.viewportCount = 1; // We have 1 dynamic one
    viewport_state_create_info.pViewports = nullptr;
    viewport_state_create_info.scissorCount = 1; // We have 1 dynamic one
    viewport_state_create_info.pScissors = nullptr;

    // Rasterization State Create Info
    VkPipelineRasterizationStateCreateInfo rasterization_state_create_info{};
    rasterization_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterization_state_create_info.pNext = nullptr;
    rasterization_state_create_info.flags = 0;
    rasterization_state_create_info.depthClampEnable = VK_FALSE;
    rasterization_state_create_info.rasterizerDiscardEnable = VK_FALSE;
    rasterization_state_create_info.polygonMode = VK_POLYGON_MODE_FILL;
    rasterization_state_create_info.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterization_state_create_info.frontFace = VK_FRONT_FACE_CLOCKWISE;
    rasterization_state_create_info.depthBiasEnable = VK_FALSE;
    rasterization_state_create_info.depthBiasConstantFactor = 0.0f;
    rasterization_state_create_info.depthBiasClamp = 0.0f;
    rasterization_state_create_info.depthBiasSlopeFactor = 0.0f;
    rasterization_state_create_info.lineWidth = 1.0f;

    // Multisample State Create Info
    VkPipelineMultisampleStateCreateInfo multisample_state_create_info{};
    multisample_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisample_state_create_info.pNext = nullptr;
    multisample_state_create_info.flags = 0;
    multisample_state_create_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisample_state_create_info.sampleShadingEnable = VK_FALSE;
    multisample_state_create_info.minSampleShading = 1.0f;
    multisample_state_create_info.pSampleMask = nullptr;
    multisample_state_create_info.alphaToCoverageEnable = VK_FALSE;
    multisample_state_create_info.alphaToOneEnable = VK_FALSE;

    // Blend Attachment State
    VkPipelineColorBlendAttachmentState color_blend_attachment_state{};
    color_blend_attachment_state.blendEnable = VK_FALSE;
    color_blend_attachment_state.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
    color_blend_attachment_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    color_blend_attachment_state.colorBlendOp = VK_BLEND_OP_ADD;
    color_blend_attachment_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    color_blend_attachment_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    color_blend_attachment_state.alphaBlendOp = VK_BLEND_OP_ADD;
    color_blend_attachment_state.colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    ;

    // Blend State Create Info
    VkPipelineColorBlendStateCreateInfo color_blend_state_create_info{};
    color_blend_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    color_blend_state_create_info.pNext = nullptr;
    color_blend_state_create_info.flags = 0;
    color_blend_state_create_info.logicOpEnable = VK_FALSE;
    color_blend_state_create_info.logicOp = VK_LOGIC_OP_COPY;
    color_blend_state_create_info.attachmentCount = 1;
    color_blend_state_create_info.pAttachments = &color_blend_attachment_state;
    color_blend_state_create_info.blendConstants[0] = 0.0f;
    color_blend_state_create_info.blendConstants[1] = 0.0f;
    color_blend_state_create_info.blendConstants[2] = 0.0f;
    color_blend_state_create_info.blendConstants[3] = 0.0f;

    /* --------- Pipeline Layout --------- */
    // Create Pipeline Layout
    VkPipelineLayoutCreateInfo pipeline_layout_create_info{};
    pipeline_layout_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipeline_layout_create_info.pNext = nullptr;
    pipeline_layout_create_info.flags = 0;
    pipeline_layout_create_info.setLayoutCount = 0;
    pipeline_layout_create_info.pSetLayouts = nullptr;
    pipeline_layout_create_info.pushConstantRangeCount = 0;
    pipeline_layout_create_info.pPushConstantRanges = nullptr;

    if (vkCreatePipelineLayout(device_manager_->GetDevice(), &pipeline_layout_create_info, nullptr,
                               &pipeline_layout_) != VK_SUCCESS)
    {
        fprintf(stderr, "Error in vkCreatePipelineLayout");
        exit(EXIT_FAILURE);
    }

    /* --------- Graphics Pipeline --------- */

    // Dynamic states
    std::vector<VkDynamicState>dynamicStates = {VK_DYNAMIC_STATE_VIEWPORT,VK_DYNAMIC_STATE_SCISSOR};
    VkPipelineDynamicStateCreateInfo dynamicStateCreateInfo{};
    dynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicStateCreateInfo.pNext = nullptr;
    dynamicStateCreateInfo.flags = 0;
    dynamicStateCreateInfo.dynamicStateCount = static_cast<uint32_t>(dynamicStates.size());
    dynamicStateCreateInfo.pDynamicStates = dynamicStates.data();

    // Pipeline
    VkGraphicsPipelineCreateInfo graphics_pipeline_create_info{};
    graphics_pipeline_create_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    graphics_pipeline_create_info.pNext = nullptr;
    graphics_pipeline_create_info.flags = 0;
    graphics_pipeline_create_info.stageCount = 2;
    graphics_pipeline_create_info.pStages = shader_stages;
    graphics_pipeline_create_info.pVertexInputState = &vertex_input_info;
    graphics_pipeline_create_info.pInputAssemblyState = &assembly_input_info;
    graphics_pipeline_create_info.pTessellationState = nullptr;
    graphics_pipeline_create_info.pViewportState = &viewport_state_create_info;
    graphics_pipeline_create_info.pRasterizationState = &rasterization_state_create_info;
    graphics_pipeline_create_info.pMultisampleState = &multisample_state_create_info;
    graphics_pipeline_create_info.pDepthStencilState = nullptr;
    graphics_pipeline_create_info.pColorBlendState = &color_blend_state_create_info;
    graphics_pipeline_create_info.pDynamicState = &dynamicStateCreateInfo;
    graphics_pipeline_create_info.layout = pipeline_layout_;
    graphics_pipeline_create_info.renderPass = swapchain_manager_->GetRenderPass();
    graphics_pipeline_create_info.subpass = 0; // Index
    graphics_pipeline_create_info.basePipelineHandle = VK_NULL_HANDLE;
    graphics_pipeline_create_info.basePipelineIndex = -1;

    if (vkCreateGraphicsPipelines(device_manager_->GetDevice(), VK_NULL_HANDLE, 1, &graphics_pipeline_create_info,
                                  nullptr, &graphics_pipeline_) != VK_SUCCESS)
    {
        fprintf(stderr, "Error in vkCreateGraphicsPipelines()");
        exit(EXIT_FAILURE);
    }

    // Destroy the shader module
    vkDestroyShaderModule(device_manager_->GetDevice(), frag_shader_module, nullptr);
    vkDestroyShaderModule(device_manager_->GetDevice(), vert_shader_module, nullptr);
}

VkShaderModule EeVulkanPipelineManager::createShaderModule(const std::vector<char> &code)
{
    VkShaderModule shader_module;

    VkShaderModuleCreateInfo create_info{};
    create_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    create_info.pNext = nullptr;
    create_info.flags = 0;
    create_info.codeSize = code.size();
    create_info.pCode = reinterpret_cast<const uint32_t *>(code.data());

    if (vkCreateShaderModule(device_manager_->GetDevice(), &create_info, nullptr, &shader_module) != VK_SUCCESS)
    {
        fprintf(stderr, "Error in vkCreateShaderModule");
        exit(EXIT_FAILURE);
    }
    return shader_module;
}

std::vector<VkFramebuffer> EeVulkanPipelineManager::createFramebuffer()
{
    std::vector<VkImageView> swap_chain_image_views = swapchain_manager_->GetSwapchainImageViews();
    std::vector<VkFramebuffer> swap_chain_framebuffers(swap_chain_image_views.size());

    for (size_t i = 0; i < swap_chain_framebuffers.size(); ++i)
    {
        VkImageView attachments[] = {swap_chain_image_views[i]};

        VkFramebufferCreateInfo framebuffer_create_info{};
        framebuffer_create_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebuffer_create_info.pNext = nullptr;
        framebuffer_create_info.flags = 0;
        framebuffer_create_info.renderPass = swapchain_manager_->GetRenderPass();
        framebuffer_create_info.attachmentCount = 1;
        framebuffer_create_info.pAttachments = attachments;
        framebuffer_create_info.width = swapchain_manager_->GetExtent().width;
        framebuffer_create_info.height = swapchain_manager_->GetExtent().height;
        framebuffer_create_info.layers = 1;

        if (vkCreateFramebuffer(device_manager_->GetDevice(), &framebuffer_create_info, nullptr,
                                &swap_chain_framebuffers[i]) != VK_SUCCESS)
        {
            fprintf(stderr, "Error in vkCreateFramebuffer");
            exit(EXIT_FAILURE);
        }
    }
    return swap_chain_framebuffers;
}

/* ------------------------------------------------- Public Methods ------------------------------------------------- */
void EeVulkanPipelineManager::Bind(VkCommandBuffer commandBuffer) const
{
    // Set viewport and scissors as we use dynamic states
    VkViewport viewport{};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = static_cast<float>(swapchain_manager_->GetExtent().width);
    viewport.height = static_cast<float>(swapchain_manager_->GetExtent().height);
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    VkRect2D scissor;
    scissor.offset.x = 0;
    scissor.offset.y = 0;
    scissor.extent = swapchain_manager_->GetExtent();
    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

    // Bind pipeline
    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, graphics_pipeline_);
}

namespace
{
std::vector<char> readFile(const std::string &filename)
{
    std::ifstream file(filename, std::ios::ate | std::ios::binary);

    if (!file.is_open())
    {
        fprintf(stderr, "Error in readFile");
        exit(EXIT_FAILURE);
    }

    // Resize buffer
    size_t file_size = (size_t)file.tellg();
    std::vector<char> buffer(file_size);

    // Read data
    file.seekg(0);
    file.read(buffer.data(), file_size);
    file.close();
    return buffer;
}
} // namespace
