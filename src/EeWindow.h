// SPDX-FileCopyrightText: (C) 2021 Manuel Wassermann <manuel.wassermann97@gmail.com>
// SPDX-License-Identifier: MIT

#pragma once

#define GLFW_INCLUDE_VULKAN

#include <GLFW/glfw3.h>
#include <cstdio>
#include <cstdlib>
#include <vector>

class EeWindow
{

    /* ----------------------------------------------- Member Variables ----------------------------------------------*/
  private:
    int width_;
    int height_;
    const char *title_;

    GLFWwindow *window_;
    uint32_t glfw_extension_count_ = 0;
    const char **glfw_extensions_;
    VkSurfaceKHR surface_ = VK_NULL_HANDLE;

    /* -------------------------------------------------- Constructor ------------------------------------------------*/
  public:
    EeWindow(const int width, const int height, const char *title);
    ~EeWindow();

    EeWindow(const EeWindow &) = delete;
    void operator=(const EeWindow &) = delete;

    /* ------------------------------------------------ Private Methods ----------------------------------------------*/
    static void framebufferResizeCallback(GLFWwindow *window, int width, int height);

    /* ------------------------------------------------ Public Methods -----------------------------------------------*/
  public:
    bool ShouldClose() const;
    static void PollEvents();
    VkSurfaceKHR const &InitSurface(VkInstance &instance);
    void DestroySurface(VkInstance instance);
    VkExtent2D QueryFramebufferSize() const;

    /* ------------------------------------------------ Getter Methods -----------------------------------------------*/
    const char *GetTitle() const { return title_; }
    /**
     *
     * @returns a vector of strings with the required instance extensions
     */
    std::vector<const char *> GetInstanceExtensions() const { return std::vector<const char *>(glfw_extensions_, glfw_extensions_ + glfw_extension_count_); }
    const uint32_t GetInstanceExtensionCount() const { return glfw_extension_count_; }
    const VkSurfaceKHR &GetSurface() const { return surface_; }
};
