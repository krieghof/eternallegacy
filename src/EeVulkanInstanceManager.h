// SPDX-FileCopyrightText: (C) 2021 Manuel Wassermann <manuel.wassermann97@gmail.com>
// SPDX-License-Identifier: MIT

#pragma once

#include "EeWindow.h"

#include <vector>
#include <vulkan/vulkan.h>

class EeVulkanInstanceManager
{
    /* ----------------------------------------------- Member Variables ----------------------------------------------*/
  private:
    const char *applicationName_ = "Eternal Application";
    EeWindow *window_ = nullptr;

    VkInstance instance_ = VK_NULL_HANDLE;
    VkSurfaceKHR surface_ = VK_NULL_HANDLE;

#ifndef NDEBUG
    const bool activateValidation_ = true;
#else
    const bool activateValidation_ = false;
#endif

    const std::vector<const char *> activeLayers_ = {"VK_LAYER_KHRONOS_validation"};
    std::vector<const char *> activeExtensions_ = {};

    /* -------------------------------------------------- Constructor ------------------------------------------------*/
  public:
    EeVulkanInstanceManager(const char *applicationName, const int width, const int height);
    ~EeVulkanInstanceManager();

    EeVulkanInstanceManager(const EeVulkanInstanceManager &) = delete;
    EeVulkanInstanceManager &operator=(const EeVulkanInstanceManager &) = delete;

    /* ------------------------------------------------ Private Methods ----------------------------------------------*/
  private:
    const VkInstance createInstance();
    void areLayersAvailable();
    void areExtensionsAvailable();

    /* ------------------------------------------------ Getter Methods -----------------------------------------------*/
  public:
    const VkInstance &GetInstance() const { return instance_; }
    const VkSurfaceKHR &GetSurface() const { return surface_; }
    EeWindow *GetWindow() const { return window_; }
};
