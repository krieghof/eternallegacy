// SPDX-FileCopyrightText: (C) 2021 Manuel Wassermann <manuel.wassermann97@gmail.com>
// SPDX-License-Identifier: MIT

#pragma once

#include "EeVulkanDeviceManager.h"
#include "EeVulkanSwapchainManager.h"

#include <string>
#include <vulkan/vulkan.h>

class EeVulkanPipelineManager
{
    /* ----------------------------------------------- Member Variables ----------------------------------------------*/
  private:
    const EeVulkanSwapchainManager *swapchain_manager_;
    const EeVulkanDeviceManager *device_manager_;

    VkPipelineLayout pipeline_layout_ = VK_NULL_HANDLE;
    VkPipeline graphics_pipeline_ = VK_NULL_HANDLE;

    /* -------------------------------------------------- Constructor ------------------------------------------------*/
  public:
    EeVulkanPipelineManager(const EeVulkanSwapchainManager *swapchain_manager,
                            const EeVulkanDeviceManager *device_manager);
    ~EeVulkanPipelineManager();

    EeVulkanPipelineManager(const EeVulkanPipelineManager &) = delete;
    EeVulkanPipelineManager &operator=(EeVulkanPipelineManager &) = delete;

    /* ------------------------------------------------ Private Methods ----------------------------------------------*/
  private:
    void createGraphicsPipeline();
    VkShaderModule createShaderModule(const std::vector<char> &code);
    std::vector<VkFramebuffer> createFramebuffer();

    /* ------------------------------------------------ Public Methods -----------------------------------------------*/
  public:
    void Bind(VkCommandBuffer commandBuffer) const;

    /* ------------------------------------------------ Getter Methods -----------------------------------------------*/
  public:
    const VkPipeline &GetGraphicsPipeline() const { return graphics_pipeline_; }
};

namespace
{
std::vector<char> readFile(const std::string &filename);
}
