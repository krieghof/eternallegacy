// SPDX-FileCopyrightText: (C) 2021 Manuel Wassermann <manuel.wassermann97@gmail.com>
// SPDX-License-Identifier: MIT

#pragma once

#include "EeVulkanDeviceManager.h"
#include "EeVulkanInstanceManager.h"
#include "EeVulkanPipelineManager.h"
#include "EeVulkanSwapchainManager.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <string>
#include <vector>

class EeRenderer
{
    /* ----------------------------------------------- Member Variables ----------------------------------------------*/
  private:
    const EeWindow *window_;
    const EeVulkanDeviceManager *device_manager_;

    EeVulkanSwapchainManager *swapchain_manager_;
    EeVulkanPipelineManager *pipeline_manager_;

    std::vector<VkCommandBuffer> command_buffers_;

    /* -------------------------------------------------- Constructor ------------------------------------------------*/
  public:
    explicit EeRenderer(const EeWindow* window, const EeVulkanDeviceManager* deviceManager) : window_(window), device_manager_(deviceManager)
    {
        swapchain_manager_ = new EeVulkanSwapchainManager(window_, device_manager_);
        pipeline_manager_ = new EeVulkanPipelineManager(swapchain_manager_, device_manager_);
        command_buffers_ = createCommandBuffer(device_manager_, swapchain_manager_);
    }

    ~EeRenderer()
    {
        //TODO: Move this in the correct destructor of one of the managers
        // When we are going to remove the lines with delete then we have just this one command here
        vkDeviceWaitIdle(device_manager_->GetDevice());

        delete pipeline_manager_;
        delete swapchain_manager_;
    }

    EeRenderer(const EeRenderer &) = delete;
    EeRenderer &operator=(const EeRenderer &) = delete;

    /* ------------------------------------------------ Private Methods ----------------------------------------------*/
  private:
    static std::vector<VkCommandBuffer> createCommandBuffer(const EeVulkanDeviceManager *deviceManager,
                                                            const EeVulkanSwapchainManager *swapChainManager)
    {
        std::vector<VkCommandBuffer> command_buffers(swapChainManager->GetFramebuffers().size());

        VkCommandBufferAllocateInfo command_buffer_allocate_info{};
        command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        command_buffer_allocate_info.pNext = nullptr;
        command_buffer_allocate_info.commandPool = deviceManager->GetCommandPool();
        command_buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        command_buffer_allocate_info.commandBufferCount = (uint32_t)command_buffers.size();
        if (vkAllocateCommandBuffers(deviceManager->GetDevice(), &command_buffer_allocate_info,
                                     command_buffers.data()) != VK_SUCCESS)
        {
            fprintf(stderr, "Error while creating command buffer");
            exit(EXIT_FAILURE);
        }
        return command_buffers;
    }

    static void recordCommandBuffer(std::vector<VkCommandBuffer> commandBuffers, EeVulkanPipelineManager *pipelineManager,
                                    EeVulkanSwapchainManager *swapChainManager, uint32_t &imageIndex)
    {
            VkCommandBufferBeginInfo begin_info{};
            begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            begin_info.pNext = nullptr;
            begin_info.flags = 0;
            begin_info.pInheritanceInfo = nullptr;
            if (vkBeginCommandBuffer(commandBuffers[imageIndex], &begin_info) != VK_SUCCESS)
            {
                fprintf(stderr, "Error in vkBeginCommandBuffer");
                exit(EXIT_FAILURE);
            }

            /* Render Pass */
            VkRenderPassBeginInfo render_pass_begin_info{};
            render_pass_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            render_pass_begin_info.pNext = nullptr;
            render_pass_begin_info.renderPass = swapChainManager->GetRenderPass();
            render_pass_begin_info.framebuffer = swapChainManager->GetFramebuffers()[imageIndex];
            render_pass_begin_info.renderArea.offset = {0, 0};
            render_pass_begin_info.renderArea.extent = swapChainManager->GetExtent();
            VkClearValue clear_color = {{{0.0f, 0.0f, 0.0f, 1.0f}}};
            render_pass_begin_info.clearValueCount = 1;
            render_pass_begin_info.pClearValues = &clear_color;

            // Begin
            vkCmdBeginRenderPass(commandBuffers[imageIndex], &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);

            // Command
            pipelineManager->Bind(commandBuffers[imageIndex]);

            vkCmdDraw(commandBuffers[imageIndex], 3, 1, 0, 0);

            // End
            vkCmdEndRenderPass(commandBuffers[imageIndex]);
            if (vkEndCommandBuffer(commandBuffers[imageIndex]) != VK_SUCCESS)
            {
                fprintf(stderr, "Error in vkEndCommandBuffer");
                exit(EXIT_FAILURE);
            }
    }

    /* ------------------------------------------------ Public Methods -----------------------------------------------*/
  public:
    void Run()
    {
        while (!window_->ShouldClose())
        {
            EeWindow::PollEvents();
            Draw();
        }
    }

    void Draw()
    {
        uint32_t image_index;
        swapchain_manager_->AcquireNextImage(image_index);

        // We record the command buffer every frame
        // NOTE: The render pass begin info is dependent of the render pass, framebuffers and extent
        // it is necessary to re record the command buffer when those change, so
        recordCommandBuffer(command_buffers_,pipeline_manager_,swapchain_manager_,image_index);
        swapchain_manager_->SubmitCommandBuffers(command_buffers_[image_index], image_index);
    }
};
