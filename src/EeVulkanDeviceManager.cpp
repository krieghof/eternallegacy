// SPDX-FileCopyrightText: (C) 2021 Manuel Wassermann <manuel.wassermann97@gmail.com>
// SPDX-License-Identifier: MIT

#include "EeVulkanDeviceManager.h"

/* ------------------------------------------------- Private Methods ------------------------------------------------ */
std::vector<EeVulkanDeviceManager::DeviceInfo> EeVulkanDeviceManager::queryPhysicalDevices(const VkInstance &instance)
{
    // Enumerate Physical Devices
    uint32_t physicalDeviceCount;
    vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, nullptr);
    VkPhysicalDevice devices[physicalDeviceCount];

    if (physicalDeviceCount == 0)
    {
        fprintf(stderr, "No Gpu with Vulkan support");
        exit(EXIT_FAILURE);
    }

    if (vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, devices) != VK_SUCCESS)
    {
        fprintf(stderr, "Error while enumerating physical devices");
        exit(EXIT_FAILURE);
    }

    // Save to struct
    std::vector<DeviceInfo> device_infos(physicalDeviceCount);
    for (size_t i = 0; i < device_infos.size(); ++i)
    {
        device_infos[i].device = devices[i];
    }

    return device_infos;
}

void EeVulkanDeviceManager::queryPhysicalDevicesInfos(const VkSurfaceKHR &surface)
{
    // TODO: Error handling check VkResult
    // Get physical device properties, features and queue family properties
    for (auto &device : devices_)
    {
        // Device Properties and Features
        vkGetPhysicalDeviceProperties(device.device, &device.properties);
        vkGetPhysicalDeviceFeatures(device.device, &device.features);

        // Queue Family Properties
        uint32_t queue_family_count;
        vkGetPhysicalDeviceQueueFamilyProperties(device.device, &queue_family_count, nullptr);
        device.queue_family_properties.resize(queue_family_count);
        vkGetPhysicalDeviceQueueFamilyProperties(device.device, &queue_family_count,
                                                 device.queue_family_properties.data());

        // Device Extension Properties
        uint32_t extension_count;
        vkEnumerateDeviceExtensionProperties(device.device, nullptr, &extension_count, nullptr);
        device.extension_properties.resize(extension_count);
        vkEnumerateDeviceExtensionProperties(device.device, nullptr, &extension_count,
                                             device.extension_properties.data());

        // Surface Infos
        if (surface != VK_NULL_HANDLE)
        {
            // Queue Family Presentation Support
            device.queue_family_presentation_support.resize(queue_family_count);
            for (int j = 0; j < queue_family_count; ++j)
            {
                VkBool32 present_support = false;
                vkGetPhysicalDeviceSurfaceSupportKHR(device.device, j, surface, &present_support);
                device.queue_family_presentation_support[j] = present_support;
            }

            // Get Surface Capabilities
            vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device.device, surface, &device.surface_capabilities);

            // Get Surface Formats
            uint32_t format_count;
            vkGetPhysicalDeviceSurfaceFormatsKHR(device.device, surface, &format_count, nullptr);
            if (format_count != 0)
            {
                device.surface_formats.resize(format_count);
                vkGetPhysicalDeviceSurfaceFormatsKHR(device.device, surface, &format_count,
                                                     device.surface_formats.data());
            }

            // Get Supported Presentation Modes
            uint32_t present_mode_count;
            vkGetPhysicalDeviceSurfacePresentModesKHR(device.device, surface, &present_mode_count, nullptr);
            if (present_mode_count != 0)
            {
                device.surface_present_modes.resize(present_mode_count);
                vkGetPhysicalDeviceSurfacePresentModesKHR(device.device, surface, &present_mode_count,
                                                          device.surface_present_modes.data());
            }
        }
    }
}

const uint32_t EeVulkanDeviceManager::getDefaultPhysicalDeviceIndex(const VkSurfaceKHR &surface) const
{
    if (surface != VK_NULL_HANDLE)
    {
        // Search a device that has a queue family with presentation support
        uint32_t index = 0;
        for (auto device : devices_)
        {
            for (auto support : device.queue_family_presentation_support)
            {
                if (support)
                {
                    return index;
                }
            }
            ++index;
        }
        fprintf(stderr, "No suitable device found");
        exit(EXIT_FAILURE);
    }
    return 0;
}

const uint32_t EeVulkanDeviceManager::getDefaultQueueFamilyIndex(const VkSurfaceKHR &surface) const
{
    if (surface != VK_NULL_HANDLE)
    {
        // Search a queue family with presentation support
        uint32_t index = 0;
        for (auto support : devices_[device_index_].queue_family_presentation_support)
        {
            if (support)
            {
                return index;
            }
            ++index;
        }
        fprintf(stderr, "No suitable queue family found");
        exit(EXIT_FAILURE);
    }
    return 0;
}

VkDevice EeVulkanDeviceManager::createDevice()
{
    VkDevice device = VK_NULL_HANDLE;

    // TODO: Maybe make activeExtensions configurable
    const std::vector<const char *> activeExtensions = {"VK_KHR_swapchain"};

    /* Device Queue Create Info */
    VkDeviceQueueCreateInfo queueCreateInfo{};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.pNext = nullptr;
    queueCreateInfo.flags = 0;
    queueCreateInfo.queueFamilyIndex = queue_family_index_;
    queueCreateInfo.queueCount = 1;
    float queuePriority = 1.0f;
    queueCreateInfo.pQueuePriorities = &queuePriority;

    /* Device Create Info */
    // TODO: Implement layers and extensions for the device creation
    VkDeviceCreateInfo deviceCreateInfo{};
    deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreateInfo.pNext = nullptr;
    deviceCreateInfo.flags = 0;
    deviceCreateInfo.queueCreateInfoCount = 1;
    deviceCreateInfo.pQueueCreateInfos = &queueCreateInfo;
    deviceCreateInfo.enabledLayerCount = 0;
    deviceCreateInfo.ppEnabledLayerNames = nullptr;
    // TODO: Test if the device does support the extensions why try to activate
    deviceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(activeExtensions.size());
    deviceCreateInfo.ppEnabledExtensionNames = activeExtensions.data();
    deviceCreateInfo.pEnabledFeatures = nullptr;

    if (vkCreateDevice(devices_[device_index_].device, &deviceCreateInfo, nullptr, &device))
    {
        fprintf(stderr, "Error while creating device");
        exit(EXIT_FAILURE);
    }
    return device;
}

/* ------------------------------------------------ Public Methods ---------------------------------------------------*/
VkCommandPool EeVulkanDeviceManager::createCommandPool()
{
    VkCommandPoolCreateInfo command_pool_create_info{};
    command_pool_create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    command_pool_create_info.pNext = nullptr;
    command_pool_create_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    command_pool_create_info.queueFamilyIndex = queue_family_index_;
    if (vkCreateCommandPool(logical_device_, &command_pool_create_info, nullptr, &commandPool_) !=
    VK_SUCCESS)
    {
        fprintf(stderr, "Error while creating command pool");
        exit(EXIT_FAILURE);
    }
    return commandPool_;
}
