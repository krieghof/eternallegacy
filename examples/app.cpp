// SPDX-FileCopyrightText: (C) 2021 Manuel Wassermann <manuel.wassermann97@gmail.com>
// SPDX-License-Identifier: MIT

#include "Eternal.h"

int main(int argc, char *argv[])
{
    auto *ee = new Eternal();

    ee->Run();

    delete ee;
}
