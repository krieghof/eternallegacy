# SPDX-FileCopyrightText: (C) 2021 Manuel Wassermann <manuel.wassermann97@gmail.com>
# SPDX-License-Identifier: MIT

project(Demo)
add_executable(demo app.cpp)

target_link_libraries (demo LINK_PUBLIC Eternal)
